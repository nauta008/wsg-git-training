# WSG Git Training

## Install Git 
1. Download latest version from https://git-scm.com/downloads 
2. Right click on the Git-x.xx.x.exe and choose "WUR-run with administrative rights". Install Git on your machine by followinging the installation wizard. The default options should be fine for most users. 
3. Now open a Command Prompt. You can use either Git Bash or Windows Command Prompt. To open Windows Command prompt:
    1. Press the Windows button on your keyboard.
    2. Type `cmd` and press Enter.

4. Type `git --version` in your command window to see wether Git is installed correctly. It should print something like:

    ``` console
    > git --version
    git version 2.28.0.windows.1
    ```
5. Type `git config --global user.name "Your Name"` to set your name. Next type `git config --global user.email "Your E-Mail"` to set your e-mail. If it is done correctly `git config user.name` and `git config user.email` should show something like:

    ```console
    > git config user.name
    Lisanne Nauta
    > git config user.email
    lisanne.nauta@wur.nl
    ```
6. Now you are ready to user Git!

